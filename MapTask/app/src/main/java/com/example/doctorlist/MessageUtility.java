package com.example.doctorlist;

import android.app.Activity;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

public class MessageUtility {

    public static final int RED_COLOR=0;
    public static final int GREEN_COLOR=1;
    public static final int ORANGE_COLOR=2;

    public static void ShowRedSnackbar(Activity activity,String msg,int color){
        Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), msg, Snackbar.LENGTH_LONG);
        View snackbarView = snackbar.getView();
        if(color==RED_COLOR){
            snackbarView.setBackgroundColor(activity.getResources().getColor(android.R.color.holo_red_light));
        }
        else if(color==ORANGE_COLOR){
            snackbarView.setBackgroundColor(activity.getResources().getColor(android.R.color.holo_orange_dark));
        }
        else if(color==GREEN_COLOR){
            snackbarView.setBackgroundColor(activity.getResources().getColor(android.R.color.holo_green_light));
        }

        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }


}
